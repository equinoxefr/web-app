package main

import (
	"fmt"
	"syscall/js"
)

var ciProjectId string
var ciMergeRequestIid string
var ciProjectPath string
var ciCommitRefSlug string

func Html(_ js.Value, args []js.Value) interface{} {

	htmlPage := fmt.Sprintf(`
	<!doctype html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="x-ua-compatible" content="ie=edge">
			<title>Hello World!</title>
			<meta name="description" content="">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			
			<style>
				.container { min-height: 100vh; display: flex; justify-content: center; align-items: center; text-align: center; }
				.title { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; display: block; font-weight: 300; font-size: 100px; color: #35495e; letter-spacing: 1px; }
				.subtitle { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; font-weight: 300; font-size: 42px; color: #526488; word-spacing: 5px; padding-bottom: 15px; }
				.links { padding-top: 15px; }
			</style>

		</head>
		<body>
			<section class="container">
				<div>
					<h1 class="title">
					👋 Hello Pierre 🌍
					</h1>
					<h2 class="subtitle">
					made with 💜 and WebAssembly
					</h2>    
					<h3 class="subtitle">
					made with 💙 and GoLang
					</h3>            
				</div>
			</section>
			<script>
				let projectId = "%v"
				let mergeRequestId = "%v"
				let projectPath = "%v"
				let commitRefSlug = "%v";
				
				document.querySelector("h3").innerText = "🖐 branch: " + commitRefSlug
			</script>
		</body> 
	</html>  	
	`, ciProjectId, ciMergeRequestIid, ciProjectPath, ciCommitRefSlug)
	return htmlPage
}

func main() {

	js.Global().Set("Html", js.FuncOf(Html))

	<-make(chan bool)
}
